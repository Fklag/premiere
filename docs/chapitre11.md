---
title:  Chapitre 11  algorithmes de tri
---


Cours de Pierre Duclosson.



## Cours 

* [Cours version pdf](chapitre11/Cours_11_tris.pdf)
* [Exercices supplémentaires](chapitre11/Cours_11_exo_sup.pdf)


## Tutoriels vidéo 

* [Algorithmes de tri par Pierre Marquestaut](https://peertube.lyceeconnecte.fr/videos/watch/cffa5c51-e0fa-4ef7-9437-743a683fc937)
